import 'package:flutter/material.dart';
import 'package:test_project/core/ui/constants/dimensions.dart';

extension Gap on SizedBox {
  static SizedBox statusBar(BuildContext context) => SizedBox(
        height: MediaQuery.of(context).padding.top,
      );
  static SizedBox bottomInsets(BuildContext context) => SizedBox(
        height: MediaQuery.of(context).padding.bottom,
      );
  static const SizedBox zero = SizedBox.shrink();
  // vertical padding
  static const SizedBox h_25 = SizedBox(height: Dimensions.verticalSpace * 0.25);
  static const SizedBox h_5 = SizedBox(height: Dimensions.verticalSpace * 0.5);
  static const SizedBox h_7 = SizedBox(height: Dimensions.verticalSpace * 0.7);
  static const SizedBox h_75 = SizedBox(height: Dimensions.verticalSpace * 0.75);
  static const SizedBox h = SizedBox(height: Dimensions.verticalSpace);
  static const SizedBox h1_25 = SizedBox(height: Dimensions.verticalSpace * 1.25);
  static const SizedBox h1_5 = SizedBox(height: Dimensions.verticalSpace * 1.5);
  static const SizedBox h2 = SizedBox(height: Dimensions.verticalSpace * 2);
  static const SizedBox h2_4 = SizedBox(height: Dimensions.verticalSpace * 2.4);
  static const SizedBox h3_5 = SizedBox(height: Dimensions.verticalSpace * 3.5);
  static const SizedBox h4 = SizedBox(height: Dimensions.verticalSpace * 4);

  // horizontal padding
  static const SizedBox w_25 = SizedBox(width: Dimensions.horizontalSpace * 0.25);
  static const SizedBox w_5 = SizedBox(width: Dimensions.horizontalSpace * 0.5);
  static const SizedBox w_75 = SizedBox(width: Dimensions.horizontalSpace * 0.75);
  static const SizedBox w = SizedBox(width: Dimensions.horizontalSpace);
  static const SizedBox w1_5 = SizedBox(width: Dimensions.horizontalSpace * 1.5);
  static const SizedBox w2 = SizedBox(width: Dimensions.horizontalSpace * 2);
  static const SizedBox w4 = SizedBox(width: Dimensions.horizontalSpace * 4);
}
