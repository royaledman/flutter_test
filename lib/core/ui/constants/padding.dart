import 'package:flutter/widgets.dart';
import 'package:test_project/core/ui/constants/dimensions.dart';

extension Insets on EdgeInsets {
  static const EdgeInsets zero = EdgeInsets.zero;

  // all

  static const EdgeInsets a_25 = EdgeInsets.all(Dimensions.padding * 0.25);
  static const EdgeInsets a_5 = EdgeInsets.all(Dimensions.padding * 0.5);
  static const EdgeInsets a_75 = EdgeInsets.all(Dimensions.padding * 0.75);
  static const EdgeInsets a = EdgeInsets.all(Dimensions.padding);
  static const EdgeInsets a2 = EdgeInsets.all(Dimensions.padding * 2);
  static const EdgeInsets a4 = EdgeInsets.all(Dimensions.padding * 4);

  // width

  static const EdgeInsets w_25 =
      EdgeInsets.symmetric(horizontal: Dimensions.horizontalPadding * 0.25);
  static const EdgeInsets w_5 =
      EdgeInsets.symmetric(horizontal: Dimensions.horizontalPadding * 0.5);
  static const EdgeInsets w_75 =
      EdgeInsets.symmetric(horizontal: Dimensions.horizontalPadding * 0.75);
  static const EdgeInsets w =
      EdgeInsets.symmetric(horizontal: Dimensions.horizontalPadding);
  static const EdgeInsets w2 =
      EdgeInsets.symmetric(horizontal: Dimensions.horizontalPadding * 2);
  static const EdgeInsets w4 =
      EdgeInsets.symmetric(horizontal: Dimensions.horizontalPadding * 4);
  static const EdgeInsets w6 =
      EdgeInsets.symmetric(horizontal: Dimensions.horizontalPadding * 6);

  // height

  static const EdgeInsets h_25 =
      EdgeInsets.symmetric(vertical: Dimensions.verticalPadding * 0.25);
  static const EdgeInsets h_5 =
      EdgeInsets.symmetric(vertical: Dimensions.verticalPadding * 0.5);
  static const EdgeInsets h_75 =
      EdgeInsets.symmetric(vertical: Dimensions.verticalPadding * 0.75);
  static const EdgeInsets h =
      EdgeInsets.symmetric(vertical: Dimensions.verticalPadding);
  static const EdgeInsets h1_5 =
      EdgeInsets.symmetric(vertical: Dimensions.verticalPadding * 1.5);
  static const EdgeInsets h2 =
      EdgeInsets.symmetric(vertical: Dimensions.verticalPadding * 2);
  static const EdgeInsets h4 =
      EdgeInsets.symmetric(vertical: Dimensions.verticalPadding * 4);
}
