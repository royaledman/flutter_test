import 'package:auto_route/auto_route.dart';
import 'package:flutter/material.dart';
import 'package:test_project/navigation/routes.dart';
import 'package:test_project/presentation/main_page.dart';

part 'router.gr.dart';

@MaterialAutoRouter(
  replaceInRouteName: 'Page,Route',
  routes: <AutoRoute>[
    AutoRoute(path: Routes.main, page: MainPage),
  ],
)
class AppRouter extends _$AppRouter {}
